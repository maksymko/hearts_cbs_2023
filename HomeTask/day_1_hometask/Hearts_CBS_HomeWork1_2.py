# за допомогою тернарного оператора релізувати логіку:
# є параметри x та у,
# якщо x < y - друкуємо x + y,
# якщо x == y - друкуємо 0,
# якщо x > y - друкуємо x - y,
# якщо x == 0 та y == 0 друкуємо "game over"


def calculate_result():
    x = int(input("Введіть значення x: "))
    y = int(input("Введіть значення y: "))

    result = "game over" if x == 0 and y == 0 else (x + y if x < y else (0 if x == y else (x - y if x > y else "game over")))

    print(result)

calculate_result()