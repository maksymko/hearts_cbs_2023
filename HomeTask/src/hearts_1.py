import random


SUITS = "♠ ♡ ♢ ♣".split()
RANK = "2 3 4 5 6 7 8 9 10 J Q K A".split()

def get_deck(shuffle=False):
    deck=[(s,r) for r in RANK for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck

def deal_hands(deck):
    return deck[0::4], deck[1::4], deck[2::4], deck[3::4]

def play():
    deck = get_deck(shuffle=False)
    names = "P1 P2 P3 P4".split()
    hands = {n: h for n, h in zip(names, deal_hands(deck))}

    for name, hand in hands.items():
        card_str = " ".join(f"{s}{r}" for (s,r) in hand)
        print(f"{name}: {card_str}") 
    

if __name__ == __main__:
    pass
    #play()