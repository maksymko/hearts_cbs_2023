import random
from typing import List, Sequence, Tuple, Any, TypeVar, Optional

SUITS = "♠ ♡ ♢ ♣".split()
RANK = "2 3 4 5 6 7 8 9 10 J Q K A".split()

Card = Tuple[str, str]
Deck = List[Card]

Choosable = TypeVar("Choosable")

def get_deck(shuffle:bool=False) -> Deck:
    deck=[(s,r) for r in RANK for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck

def choose(items: Sequence[Choosable]) -> Choosable:
    return random.choice(items)

def player_order(names: List[str], start: Optional[str] = None) -> List[str]:
    if start is None:
        start = choose(names)
    start_idx = names.index(start)
    return names[start_idx:] + names[:start_idx]



def deal_hands(deck: Deck) -> Tuple[Deck, Deck, Deck, Deck]:
    return deck[0::4], deck[1::4], deck[2::4], deck[3::4]

def play():
    deck = get_deck(shuffle=False)
    names = "P1 P2 P3 P4".split()
    hands: dict[str, Deck] = {n: h for n, h in zip(names, deal_hands(deck))}
    start_player = choose(names)
    turn_player = player_order(names, start=start_player)
    
    while  hands(start_player):
        for name in turn_player:
            card = choose(hands[name])
            hands[name].remove(card)
            print(f"{name}: {card[0]+card[1]:<3}", end='')
        print()

    for name, hand in hands.items():
        card_str = " ".join(f"{s}{r}" for (s,r) in hand)
        print(f"{name}: {card_str}") 
    

if __name__ == __main__:
    pass
    #play()