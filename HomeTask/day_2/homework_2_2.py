# за допомогою тернарного оператора релізувати логіку:
# є параметри x та у,
# якщо x < y - друкуємо x + y,
# якщо x == y - друкуємо 0,
# якщо x > y - друкуємо x - y,
# якщо x == 0 та y == 0 друкуємо "game over"


def calculate_result():
    x = int(input("Введіть значення x: "))
    y = int(input("Введіть значення y: "))

    result = "game over" if x == 0 and y == 0 else (x + y if x < y else (0 if x == y else (x - y if x > y else "game over")))

    print(result)

calculate_result()

#За матеріалом лекції
""" if __name__ == "__main__":
    cases = (
        (1, 2, "3"),
        (1, 1, "0"),
        (2, 1, "3"),
        (0, 0, "game over"),
    )
    for x, y, result in cases:
        func_res = get_ternary(x, y)
        assert (
            func_res == result
        ), f"ERROR: get_ternary({x}, {y}) returned {func_res}, but expected: {result}"

    print(get_symbols_frequency("1234sdfghjkjhgfdsdfghjk   rrrrrrrrrrrrrrr"))
 """