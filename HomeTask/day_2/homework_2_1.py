# встановити git та зареєструвати акк на gitlab
# написати функцію якам приймає рядок і повертає словник у якому
# ключами є всі символи, які зустрічаються в цьому рядку, а значення - відповідні
# вірогідності зустріти цей символ в цьому рядку.
# № код повинен бути структурований за допомогою конструкції if name == "main":,
# всі аргументи і значення що функція повертає повинні бути типізовані, функція має рядок документації

def calculate_character_probabilities(input_string: str) -> dict:
    """
    Обчислює ймовірності появи кожного символу у вхідному рядку.

    Аргументи:
    - input_string (str): Вхідний рядок, для якого потрібно обчислити ймовірності.

    Результат:
    - dict: Словник, де ключами є символи, а значеннями - їхні ймовірності.

    Example:
    >>> calculate_character_probabilities("hello")
    {'h': 0.2, 'e': 0.2, 'l': 0.4, 'o': 0.2}
    """
    character_count = {}
    total_characters = len(input_string)

    # Підрахунок входження кожного символу
    for char in input_string:
        if char in character_count:
            character_count[char] += 1
        else:
            character_count[char] = 1

    # Обчислюємо ймовірності
    probabilities = {char: count / total_characters for char, count in character_count.items()}

    return probabilities

if __name__ == "__main__":
    input_str = input("Введіть рядок: ")
    probabilities_dict = calculate_character_probabilities(input_str)
    print("Character Probabilities:")
    for char, prob in probabilities_dict.items():
        print(f"{char}: {prob:.2f}")
